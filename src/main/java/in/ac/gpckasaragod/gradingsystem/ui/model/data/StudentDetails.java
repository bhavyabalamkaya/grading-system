/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.gradingsystem.ui.model.data;

import java.util.Objects;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class StudentDetails {
    private Integer id;
    private String studentName;
    private String registerNumber;
    private String nameofSchool;
    private String admissionNumber;
    private String fathersName;
    private String mothersName;
    private String address;

    public StudentDetails(Integer id, String studentName, String registerNumber, String nameofSchool, String admissionNumber, String fathersName, String mothersName, String address) {
        this.id = id;
        this.studentName = studentName;
        this.registerNumber = registerNumber;
        this.nameofSchool = nameofSchool;
        this.admissionNumber = admissionNumber;
        this.fathersName = fathersName;
        this.mothersName = mothersName;
        this.address = address;
    }

    @Override
    public String toString() {
        return  studentName + "- RegNo=" + registerNumber + "- AdmNo=" + admissionNumber + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StudentDetails other = (StudentDetails) obj;
        return Objects.equals(this.id, other.id);
    }
    
    
    

    public Integer getId() {
        return id;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getRegisterNumber() {
        return registerNumber;
    }

    public String getNameofSchool() {
        return nameofSchool;
    }

    public String getAdmissionNumber() {
        return admissionNumber;
    }

    public String getFathersName() {
        return fathersName;
    }

    public String getMothersName() {
        return mothersName;
    }

    public String getAddress() {
        return address;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber;
    }

    public void setNameofSchool(String nameofSchool) {
        this.nameofSchool = nameofSchool;
    }

    public void setAdmissionNumber(String admissionNumber) {
        this.admissionNumber = admissionNumber;
    }

    public void setFathersName(String fathersName) {
        this.fathersName = fathersName;
    }

    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}