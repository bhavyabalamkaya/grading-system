/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.gradingsystem.ui.model.data;

/**
 *
 * @author student
 */
public class GradingDetails {
    private Integer id;
    private Integer studentId;
    
    private Integer english;
    private Integer maths;
    private Integer physics;
    private Integer computer;
    private Integer science;
    private Integer total;
    private Integer average;
    private String grade;

    public GradingDetails(Integer id, Integer studentId, Integer english, Integer maths, Integer physics, Integer computer, Integer science, Integer total, Integer average, String grade) {
        this.id = id;
        this.studentId = studentId;
        this.english = english;
        this.maths = maths;
        this.physics = physics;
        this.computer = computer;
        this.science = science;
        this.total = total;
        this.average = average;
        this.grade = grade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getEnglish() {
        return english;
    }

    public void setEnglish(Integer english) {
        this.english = english;
    }

    public Integer getMaths() {
        return maths;
    }

    public void setMaths(Integer maths) {
        this.maths = maths;
    }

    public Integer getPhysics() {
        return physics;
    }

    public void setPhysics(Integer physics) {
        this.physics = physics;
    }

    public Integer getComputer() {
        return computer;
    }

    public void setComputer(Integer computer) {
        this.computer = computer;
    }

    public Integer getScience() {
        return science;
    }

    public void setScience(Integer science) {
        this.science = science;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getAverage() {
        return average;
    }

    public void setAverage(Integer average) {
        this.average = average;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    
}
