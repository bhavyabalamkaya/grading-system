/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.gradingsystem.service.impl;

import in.ac.gpckasaragod.gradingsystem.ui.model.data.StudentDetails;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import in.ac.gpckasaragod.gradingsystem.service.StudentService;
import java.sql.PreparedStatement;

/**
 *
 * @author student
 */
public class StudentServiceImpl extends ConnectionServiceImpl implements StudentService {
    public String saveStudentdetails(String studentName, String registerNumber, String nameofSchool, String admissionNumber, String fathersName, String mothersName,String address) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO STUDENT_DETAILS (STUDENT_NAME,REGISTER_NUMBER,NAME_OF_SCHOOL,ADMISSION_NUMBER,FATHERS_NAME,MOTHERS_NAME,ADDRESS) VALUES" + "('"+studentName+"','"+registerNumber+"','"+nameofSchool+"','"+admissionNumber+"','"+fathersName+"','"+mothersName+"','"+address+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                  return "save failed";
            }else{
                  return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "save failed";
   
        }
    
}
    public StudentDetails readstudentDetails(Integer id){
        StudentDetails studentDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM STUDENT_DETAILS WHERE ID="+id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String studentName = resultSet.getString("STUDENT_NAME");
                String registerNumber = resultSet.getString("REGISTER_NUMBER");
                String nameofSchool = resultSet.getString("NAME_OF_SCHOOL");
                String admissionNumber = resultSet.getString("ADMISSION_NUMBER");
                String fathersName = resultSet.getString("FATHERS_NAME");
                String mothersName = resultSet.getString("MOTHERS_NAME");
                String address = resultSet.getString("ADDRESS");
                studentDetails = new StudentDetails(id,studentName,registerNumber,nameofSchool,admissionNumber,fathersName,mothersName,address);
                
            }
        }catch (SQLException ex){
            Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
        }
        return studentDetails;
    }    
        
    public List<StudentDetails> getAllstudentDetails(){
        List<StudentDetails> studentDetails = new ArrayList<>();
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM STUDENT_DETAILS";
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String studentName = resultSet.getString("STUDENT_NAME");
                String registerNumber = resultSet.getString("REGISTER_NUMBER");
                String nameofSchool = resultSet.getString("NAME_OF_SCHOOL");
                String admissionNumber = resultSet.getString("ADMISSION_NUMBER");
                String fathersName = resultSet.getString("FATHERS_NAME");
                String mothersName = resultSet.getString("MOTHERS_NAME");
                String address = resultSet.getString("ADDRESS");
                StudentDetails studentDetail = new StudentDetails(id,studentName,registerNumber,nameofSchool,admissionNumber,fathersName,mothersName,address);
                studentDetails.add(studentDetail);
                
            }
        }catch(SQLException ex) {
            Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            
        }
           return studentDetails;
    }
    
    @Override
    public String updateStudentDetails(Integer id, String studentName, String registerNumber, String nameofSchool, String admissionNumber, String fathersName, String mothersName,String address){
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE STUDENT_DETAILS SET STUDENT_NAME='"+studentName+"',REGISTER_NUMBER='"+registerNumber+"',NAME_OF_SCHOOL='"+nameofSchool+"',ADMISSION_NUMBER='"+admissionNumber+"',FATHERS_NAME='"+fathersName+"',MOTHERS_NAME='"+mothersName+"',ADDRESS='"+address+"' WHERE ID="+id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
                return "Update failed";
            else
                return "Update successfully";
            }catch(SQLException ex){
                //Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            }
                return "Update failed";
                
            
        }

   

    @Override
    public String deleteStudentDetails(Integer id) {
         try{
             Connection connection = getConnection();
             String query = "DELETE FROM STUDENT_DETAILS WHERE ID=?";
             PreparedStatement statement = connection.prepareCall(query);
             statement.setInt(1, id);
             int delete = statement.executeUpdate();
             if(delete !=1)
                 return "Delete failed";
             else
                 return "Deleted successfully";
         } catch (SQLException ex) {
            Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Delete failed";
        }
    
}

    @Override
    public List<StudentDetails> getAllStudentDetails() {
        List<StudentDetails> studentDetails = new ArrayList<>();
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM STUDENT_DETAILS";
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String studentName = resultSet.getString("STUDENT_NAME");
                String registerNumber = resultSet.getString("REGISTER_NUMBER");
                String nameofSchool = resultSet.getString("NAME_OF_SCHOOL");
                String admissionNumber = resultSet.getString("ADMISSION_NUMBER");
                String fathersName = resultSet.getString("FATHERS_NAME");
                String mothersName = resultSet.getString("MOTHERS_NAME");
                String address = resultSet.getString("ADDRESS");
                StudentDetails studentDetail = new StudentDetails(id,studentName,registerNumber,nameofSchool,admissionNumber,fathersName,mothersName,address);
                studentDetails.add(studentDetail);
                
            }
        }catch(SQLException ex) {
            Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            
        }
           return studentDetails;
           
           
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

   
    @Override
    public StudentDetails readStudentDetails(Integer id) {
        StudentDetails studentDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM STUDENT_DETAILS WHERE ID="+id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String studentName = resultSet.getString("STUDENT_NAME");
                String registerNumber = resultSet.getString("REGISTER_NUMBER");
                String nameofSchool = resultSet.getString("NAME_OF_SCHOOL");
                String admissionNumber = resultSet.getString("ADMISSION_NUMBER");
                String fathersName = resultSet.getString("FATHERS_NAME");
                String mothersName = resultSet.getString("MOTHERS_NAME");
                String address = resultSet.getString("ADDRESS");
                studentDetails = new StudentDetails(id,studentName,registerNumber,nameofSchool,admissionNumber,fathersName,mothersName,address);
                
            }
        }catch (SQLException ex){
            Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
        }
        return studentDetails;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
    public String saveStudentDetails(String studentName, String registerNumber, String nameofSchool, String admissionNumber, String fathersName, String mothersName, String address) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO STUDENT_DETAILS (STUDENT_NAME,REGISTER_NUMBER,NAME_OF_SCHOOL,ADMISSION_NUMBER,FATHERS_NAME,MOTHERS_NAME,ADDRESS) VALUES" + "('"+studentName+"','"+registerNumber+"','"+nameofSchool+"','"+admissionNumber+"','"+fathersName+"','"+mothersName+"','"+address+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                  return "save failed";
            }else{
                  return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "save failed";
   
        }
       
        
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String saveStudentdetails(String studentName, String registerNumber, String nameofSchool, String admissionNumber, String fathersName, String mothersName) {
         try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO STUDENT_DETAILS (STUDENT_NAME,REGISTER_NUMBER,NAME_OF_SCHOOL,ADMISSION_NUMBER,FATHERS_NAME,MOTHERS_NAME,ADDRESS) VALUES" + "('"+studentName+"','"+registerNumber+"','"+nameofSchool+"','"+admissionNumber+"','"+fathersName+"','"+mothersName+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                  return "save failed";
            }else{
                  return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "save failed";
   
        }
        
//throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

   
    

  

   
}
    


    

    
   

