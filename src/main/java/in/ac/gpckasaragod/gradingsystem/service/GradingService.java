/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.gradingsystem.service;

import in.ac.gpckasaragod.gradingsystem.model.ui.GradingsystemUiModel;
import in.ac.gpckasaragod.gradingsystem.ui.model.data.GradingDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface GradingService {
   
    public List<GradingDetails> getAllGradingDetails();
    public GradingDetails readGradingDetails(Integer id);
    public String saveGradingdetails(Integer studentId, Integer english, Integer maths, Integer physics, Integer computer, Integer science, Integer total, Integer average, String grade); 
    public String deleteGradingDetails(Integer id);
    public String UpdateGradingdetails(Integer id,Integer studentId, Integer english, Integer maths, Integer physics, Integer computer, Integer science, Integer total, Integer average, String grade); 
    public List<GradingsystemUiModel> getAllGradingsDetails();
}

