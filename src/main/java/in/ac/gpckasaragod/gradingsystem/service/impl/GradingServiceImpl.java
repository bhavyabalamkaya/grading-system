/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.gradingsystem.service.impl;

import in.ac.gpckasaragod.gradingsystem.model.ui.GradingsystemUiModel;
import in.ac.gpckasaragod.gradingsystem.service.GradingService;
import in.ac.gpckasaragod.gradingsystem.ui.model.data.GradingDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class GradingServiceImpl extends ConnectionServiceImpl implements GradingService {
    @Override
    public List<GradingDetails> getAllGradingDetails() {
         //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         List<GradingDetails> gradingDetails = new ArrayList<>();
       try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM GRADING_SYSTEM";
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                Integer studentId = resultSet.getInt("STUDENT_ID");
                Integer english = resultSet.getInt("ENGLISH");
                Integer maths = resultSet.getInt("MATHS");
                Integer physics = resultSet.getInt("PHYSICS");
                Integer computer = resultSet.getInt("COMPUTER");
                Integer science = resultSet.getInt("SCIENCE");
                Integer total = resultSet.getInt("TOTAL");
                Integer average = resultSet.getInt("AVERAGE");
                String grade = resultSet.getString("GRADE");
                GradingDetails gradingDetail = new GradingDetails(id,studentId,english,maths,physics,computer,science,total,average,grade);
                gradingDetails.add(gradingDetail);
            }
                
    }   catch (SQLException ex) {
            Logger.getLogger(GradingServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
       return gradingDetails; 
    }
    
    @Override
    public List<GradingsystemUiModel> getAllGradingsDetails() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
           List<GradingsystemUiModel> gradingDetails = new ArrayList<>();
       try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT S.STUDENT_NAME,S.REGISTER_NUMBER,S.ADMISSION_NUMBER,G.ID,G.STUDENT_ID,G.ENGLISH,G.MATHS,G.PHYSICS,G.COMPUTER,G.SCIENCE,G.TOTAL,G.AVERAGE,G.GRADE FROM STUDENT_DETAILS S JOIN GRADING_SYSTEM G ON S.ID=G.STUDENT_ID;";
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String studentName = resultSet.getString("STUDENT_NAME");
                String registerNumber = resultSet.getString("REGISTER_NUMBER");
                Integer admissionNumber = resultSet.getInt("ADMISSION_NUMBER");
                Integer studentId = resultSet.getInt("STUDENT_ID");
                Integer english = resultSet.getInt("ENGLISH");
                Integer maths = resultSet.getInt("MATHS");
                Integer physics = resultSet.getInt("PHYSICS");
                Integer computer = resultSet.getInt("COMPUTER");
                Integer science = resultSet.getInt("SCIENCE");
                Integer total = resultSet.getInt("TOTAL");
                Integer average = resultSet.getInt("AVERAGE");
                String grade = resultSet.getString("GRADE");
                GradingsystemUiModel gradingDetail = new GradingsystemUiModel(id,studentName,registerNumber,admissionNumber,studentId,english,maths,physics,computer,science,total,average,grade);
                gradingDetails.add(gradingDetail);
            }
                
    }   catch (SQLException ex) {
            Logger.getLogger(GradingServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
       return gradingDetails; 
    }

    @Override
    public GradingDetails readGradingDetails(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        GradingDetails gradingDetails = null;
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM GRADING_SYSTEM WHERE ID="+id;
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()){
                Integer studentId = resultSet.getInt("STUDENT_ID");
                Integer english = resultSet.getInt("ENGLISH");
                Integer maths = resultSet.getInt("MATHS");
                Integer physics = resultSet.getInt("PHYSICS");
                Integer computer = resultSet.getInt("COMPUTER");
                Integer science = resultSet.getInt("SCIENCE");
                Integer total = resultSet.getInt("TOTAL");
                Integer average = resultSet.getInt("AVERAGE");
                String grade = resultSet.getString("GRADE");
                gradingDetails = new GradingDetails(id,studentId,english,maths,physics,computer,science,total,average,grade);
                
            }
    }catch (SQLException ex){
            Logger.getLogger(GradingServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
        }
        return gradingDetails;
    }

    @Override
    public String deleteGradingDetails(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try{
             Connection connection = getConnection();
             String query = "DELETE FROM GRADING_SYSTEM WHERE ID=?";
             PreparedStatement statement = connection.prepareStatement(query);
             statement.setInt(1, id);
             int delete = statement.executeUpdate();
             if(delete!=1)
                 return "Delete failed";
             else
                 return "Deleted successfully";
         } catch (SQLException ex) {
            Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Delete failed";
        }
    }

    @Override
    public String UpdateGradingdetails(Integer id, Integer studentId, Integer english, Integer maths, Integer physics, Integer computer, Integer science, Integer total, Integer average, String grade) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE GRADING_SYSTEM SET STUDENT_ID='"+studentId+"',ENGLISH='"+english+"',MATHS='"+maths+"',PHYSICS='"+physics+"',COMPUTER='"+computer+"',SCIENCE='"+science+"',TOTAL='"+total+"',AVERAGE='"+average+"',GRADE='"+grade+"' WHERE ID="+id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
                return "Update failed";
            else
                return "Update successfully";
            }catch(SQLException ex){
                //Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            }
                return "Update failed";
                
            
        }

    @Override
    public String saveGradingdetails(Integer studentId, Integer english, Integer maths, Integer physics, Integer computer, Integer science, Integer total, Integer average, String grade) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO GRADING_SYSTEM (STUDENT_ID,ENGLISH,MATHS,PHYSICS,COMPUTER,SCIENCE,TOTAL,AVERAGE,GRADE) VALUES" + "('"+studentId+"','"+english+"','"+maths+"','"+physics+"','"+computer+"','"+science+"','"+total+"','"+average+"','"+grade+"')";
             System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                  return "save failed";
            }else{
                  return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(GradingServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "save failed";
        }
    }

   
    }
   

    


    


    
        
    
    





    

