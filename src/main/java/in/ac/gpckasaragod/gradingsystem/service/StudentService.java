/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.gradingsystem.service;

import in.ac.gpckasaragod.gradingsystem.ui.model.data.StudentDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface StudentService{ 
     public String saveStudentdetails(String studentName, String registerNumber, String nameofSchool, String admissionNumber, String fathersName, String mothersName, String address);
     public List<StudentDetails> getAllStudentDetails();
     public StudentDetails readStudentDetails(Integer id);
     public String updateStudentDetails(Integer id, String studentName, String registerNumber, String nameofSchool, String admissionNumber, String fathersName, String mothersName,String address);
     public String deleteStudentDetails(Integer id);
     
}

